const courses = [
	{
		id: "248_c001",
		name: "Self-Defense with Sir Cardo",
		description: "Stay safe and stay alive! Immortality sold separately.",
		price: 45000,
		onOffer: true
	},
	{
		id: "248_c002",
		name: "Geology with Teacher Narda",
		description: "This course rocks!",
		price: 43000,
		onOffer: true
	},
	{
		id: "248_c003",
		name: "Rapping for Beginners with Prof. Boy Abunda",
		description: "Fast Talk Guaranteed!",
		price: 55000,
		onOffer: true
	}
]


export default courses;

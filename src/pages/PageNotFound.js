import Banner from "../components/Banner";

const PageNotFound = () =>{

   const data = {
        title: "404 - Page Not Found!",
        description: "The page you are looking for cannot be found",
        btn: "Back to Home"
    }
    return (
        <Banner dataProp={data}/>
    );
}

export default PageNotFound;

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

const Home = () =>{

  const data = {
    
        title: "B248 Booking App",
        description: "Oppurtunities for everyone, everywhere!",
        btn: "Enroll"
    }

  return (
      <>
        <Banner dataProp={data}/>
        <Highlights/>
      </>
  );
}

export default Home;

import { Row, Col, Button } from "react-bootstrap";
import {Link} from 'react-router-dom'

const Banner = ({dataProp}) => {

  console.log(dataProp);
 const {title, description, btn} = dataProp;

  return (
    <Row>
      <Col>
        <h1>{title}</h1>
        <p>{description}</p>
        <Button as={Link} to="/" variant="primary">{btn}</Button>
      </Col>
    </Row>
  );
};

export default Banner;

import NavbarApp from "./components/NavbarApp";
import CourseView from './components/CourseView';
import Home from "./pages/Home";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import PageNotFound from "./pages/PageNotFound";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router,Routes, Route, Navigate } from "react-router-dom";
import "./App.css";
import { UserProvider } from './UserContext';
import { useState, useEffect } from 'react';

const App = () => {
  
   //define a state hook 
  //this will be used to store information and will be used for for validating if a user is logged in on the app or not
  //a state hook for the user state that's defined here for a global scope
  //We created a state that will be used ALL THROUGHOUT our components in our application

  const [user, setUser] = useState({
    id: null,
    isAdmin: null});

  //for clearning out the local storage upon logging out
  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(()=>{

    fetch(`http://localhost:4000/users/details`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data=>{

      console.log(data);

      if(typeof data._id !== "undefined"){

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  },[])


  
//wrap all the components within the UserProvider to allow re-rendering when the value changes

  return (

    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <NavbarApp />
        <Container> 
          <Routes>
            <Route path="/"  element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/courses/:courseId" element={<CourseView/>}/>
            <Route path="/register" element={<Register/>} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="*" element={<PageNotFound />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>

  );

};

export default App;
